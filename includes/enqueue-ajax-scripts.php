<?php



// Enque wp scripts
function swa_enqueue_coupon_func(){
         // https://premium.wpmudev.org/blog/load-posts-ajax/

         wp_enqueue_script( 'swa-ajax-coupon-form',
               plugins_url( 'swa-ajax-coupon-form.js', __FILE__ ),
               array( 'jquery' ),
               '1.0',
                true);


          wp_localize_script( 'swa-ajax-coupon-form',
                              'swaAjaxCouponForm',
                              array(
                                'ajaxurl'     => admin_url( 'admin-ajax.php' ),
                            )
                );

 }

 add_action( 'wp_enqueue_scripts', 'swa_enqueue_coupon_func' );
