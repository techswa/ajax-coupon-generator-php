// Get the requested booklet's chapter

// This should read the navigation links at the bottom of the page
// Used to load the prior or next page to view

(function($) {


	// Disable AJAX cache
	$.ajaxSetup({cache:false});


	//create ajax spinner image
	$('<img src="/wp-content/uploads/system/ajax/ajax-spinner.gif" id="spinner" />').css('position','absolute').hide().appendTo('body');

//Get the query vars
//var foo = getParameterByName('foo');
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

//Load the coupon automatically
coupon_code = getParameterByName('ap_id');
$('#input-coupon-code').val(coupon_code);

	//for every field change
  $('#btn-coupon-code').on("click", function(event){
   	event.preventDefault();
    //get the coupon code
    var coupon_code = '';
    coupon_code = $('#input-coupon-code').val();

		//Exit if the coupon code is blank
		if (coupon_code =='' ){
			return false;
		}

    //Disallow resending data if valid coupon_code has already been applied
    if ( !$('#input-coupon-code').data('coupon-valid') ){

      //Where to display spinner
      //get element position
    		var position = $('#btn-coupon-code').offset();

      //Display spinner
      swa_ajax_coupon_form_spinner_show(position);

      //Process the ajax call
      swa_ajax_process_coupon_form(coupon_code);

      //Hide spinner
      swa_ajax_coupon_form_spinner_hide();
    }

  })

//Submit the form witht he affiliate_id
$('#coupon-code-next').on("click", function(event){
  event.preventDefault();
  //Get the affilaite_id
  var affilaite_id = $('#input-coupon-code').val();
  var nonce = $('#form-nonce').val();
  var querystring = '/member-signup?coupon_code=' + affilaite_id + '&nonce=' + nonce;
      // querystring equals "?price=100000,200000" -> exactly what I want !

      window.location.href = querystring; // <-- this should work.


})

//Submit next page with query string - affilaite_id
function submit_next_page(){

}

// Ajax was a success - updaet form elements
function swa_ajax_coupon_form_success(response){
	// if result was empty
	if (response.success == false ) {
		$('#coupon-form-group').addClass('has-error');
		$('#coupon-error').removeClass('hide');
	} else {

	  var active =response.data.active
	  var amount = response.data.amount;
	  var usage_limit = response.data.usage_limit;
	  var usage_count = response.data.usage_count;
	  var form_nonce = response.data.nonce;

		$('#coupon-form-group').removeClass('has-error');
		$('#coupon-error').hide();

	  //Add readonly attr to coupon
	  $('#input-coupon-code').attr('readonly','');

	  //Hide the apply button - not needed
	  $('#btn-coupon-code').addClass('hide');

	  //Set amount due to zero
	  $('#input-amount-due').val('0.00');

	  //Display the next screen button
	  $('#coupon-code-next').removeClass('hide');

	  //Set the form nonce
	  $('#form-nonce').val(form_nonce);
	}

}


function swa_ajax_coupon_form_error(error){
  //Display error div
  $('#coupon-error').removeClass('hide');
  //alert('function returned: ' + error.data);
}

//Display the spinner
function swa_ajax_coupon_form_spinner_show(position){

  $('#spinner').css({ top: position.top - 50, left: position.left }).fadeIn();
}

//Hide the spinner
function swa_ajax_coupon_form_spinner_hide(){
	$('#spinner').fadeOut();
}


/*
*  AJAX Code
*  Verify that the coupon is valid
*
*
*/

function swa_ajax_process_coupon_form( coupon_code ){

		$.ajax({
			url: swaAjaxCouponForm.ajaxurl,
			type: 'post',
			dataType: 'json',
			data: {
    				action: 'swa_ajax_coupon_form',
    				//Set the query vars
            coupon_code: coupon_code
			},
			success: function( response ) {
          //Update form with success results
					//alert('Ajax success: ' + JSON.stringify(response));
          swa_ajax_coupon_form_success(response);
			},
			error: function( error) {
        //Update form with success results
				//alert('Ajax failure: ' + JSON.stringify(response));
        swa_ajax_coupon_form_error(error);
			}
		})

		return true;
	}


})(jQuery);
