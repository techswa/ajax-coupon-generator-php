<?php

// WP hooks for ajax - why do we need the nonpriv?
// add_action( wp_ajax_action_name, call_back_name)
add_action( 'wp_ajax_nopriv_swa_ajax_coupon_form', 'swa_ajax_coupon_form' );
add_action( 'wp_ajax_swa_ajax_coupon_form', 'swa_ajax_coupon_form' );


// This processes the AJAX request on the server
// and returns the results

function swa_ajax_coupon_form(){

//Get the query var
$coupon_code = $_POST['coupon_code'];

//Call the database function to verify the coupon code
$coupon_data = swa_verify_coupon_code( $coupon_code );

// //Determine if we found the coupon code
// if ($coupon_data) {
//   
// } 

if ( $coupon_data ) {
  wp_send_json_success($coupon_data);
} else {
  wp_send_json_error($coupon_data);
}

  return $result;
}

// id, affilaite_id, amount, start_date, end_date, usage_limit, usageCount
//Query the coupon code and validate
function swa_verify_coupon_code( $affiliate_id ){
    
    $affilaite_id = 'CSRS0916A';
      global $wpdb;

      $query = "SELECT  id, amount, active, usage_limit, usage_count FROM swa_coupons WHERE affiliate_id = '" . $affiliate_id . "'";

      $record = $wpdb->get_row($query);
      $nonce = wp_create_nonce('member_signup_form');

      //If no coupon was found_posts

      if ( is_null($record) ){
        $data = false;
      } else {
        $data = array(
          'active'        =>  $record->active
          ,'amount'       =>  $record->amount
          ,'usage_limit'  =>  $record->usage_limit
          ,'usage_count'  =>  $record->usage_count
          ,'nonce'        =>  $nonce
        );
      }
      return $data;
  }
