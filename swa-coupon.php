<?php

/*
Plugin Name: SWA EYCS Coupon Form
Version: 1.0.0
Plugin URI:
Author: Smarter Web Apps
Author URI: http://www.smarterwebapps.com/
*/


include_once('includes/swa-ajax-coupon-form.php');
include_once('includes/enqueue-ajax-scripts.php');


function swa_display_coupon_form_func(){


$form = "
<div id='coupon-form' >
  <form class='form-verticle' role='form' >
    <div class='form-group'>
      <input type='hidden' name='form-nonce' id='form-nonce'/>
       <label for='select-course'>Course</label>
       <select id='select-course' class='form-control'/>
       <option>Home Owner</option>
       </select>
    </div>

    <div class='form-group'>
      <label class='' for='input-course-cost'>Cost</label>
        <input type='text' class='form-control' name='input-course-cost' id='input-course-cost' value='395.00' readonly='readonly'/>
    </div>

      <div id='coupon-form-group' class='form-group'>
         <label class='control-label' for='input-coupon-code'>Coupon Code</label>
         <input class='form-control' type='text' name='input-coupon-code' id='input-coupon-code' placeholder='Apply coupon'  aria-describedby='inputSuccessForCouponCode'/>
         <span class='glyphicon glyphicon-ok form-control-feedback' aria-hidden='true'></span>
         <span id='inputSuccessForCouponCode' class='sr-only'>(success)</span>
         <button type ='submit' id='btn-coupon-code' class='btn btn-primary' href='#'>Apply</button>
      <div>

      <div id='coupon-error' class='coupon-error hide'>
      The sponsor code that you entered can't be found.  Reenter it and if your are still having an issue please contact your sponsor for a replacement.
      </div>

    <div class='form-group'>
      <label class='' for='input-amount-due'>Amount due</label>
      <input type='text' class='form-control' name='input-amount-due' id='input-amount-due' value='395.00' readonly/>
    </div>

  </form>

</div>



<div id='coupon-code-next' class='btn-coupon-code-continue hide'>
<buttom type ='submit' id='btn-coupon-code' class='btn btn-primary' href='#'>Continue</button>
</div>

";

  return $form;
}

add_shortcode('display_coupon_form', 'swa_display_coupon_form_func');
