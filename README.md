# PHP AJAX Display Panel #

Generates custom display panel derived from a QueryString variable.

### Use case ###

This code is currently packaged as a Wordpress plugin, allows the developer to display custom content based upon what would typically be a referral code.

The QueryString can be a code that the user has typed into a form, a link such as from Facebook, a calculated field, or based upon the web adn page the user had just visited. 